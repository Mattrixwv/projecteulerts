//ProjectEulerTS/ProjectEuler.ts
//Matthew Ellison
// Created: 10-18-20
//Modified: 10-18-20
//This is the driver function for the Java version of the ProjectEuler project
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { ProblemSelection } from "./ProblemSelection";
import { Benchmark } from "./Benchmark";
const readlineSync = require("readline-sync");


//An enum to hold the possible menu selections
enum SELECTIONS{SOLVE = 1, DESCRIPTION, LIST, BENCHMARK, EXIT, SIZE};

class Driver{
	//Drives the program
	public static main(): void{
		//Holds the menu selection of the user
		let selection: number;
		do{
			Driver.printMenu();
			selection = Driver.getMenuSelection();

			switch(selection){
				case SELECTIONS.SOLVE: Driver.solveMenu(); break;
				case SELECTIONS.DESCRIPTION: Driver.descriptionMenu(); break;
				case SELECTIONS.LIST: ProblemSelection.listProblems(); break;
				case SELECTIONS.BENCHMARK: Benchmark.benchmarkMenu(); break;
				case SELECTIONS.EXIT: break;
				case SELECTIONS.SIZE:
				default: Driver.printErrorMessage();
			}
		}while(selection != SELECTIONS.EXIT);
	}
	//Print the menu
	public static printMenu(): void{
		console.log("1. Solve a problem");
		console.log("2. Print a problem description");
		console.log("3. List valid problem numbers");
		console.log("4. Benchmark");
		console.log("5. Exit");
		console.log();
	}
	//Get a menu selection from the user
	private static getMenuSelection(): SELECTIONS{
		let selection: number;
		let sel: string = readlineSync.question(' ');
		while(!Driver.isValidMenu(sel)){
			console.log("That is an invalid option!\n");
			selection = Driver.getMenuSelection();
		}
		selection = parseInt(sel);
		return selection;
	}
	//Make sure the value passed in is a valid menu option
	private static isValidMenu(sel: string): boolean{
		let selection: number;
		try{
			selection = parseInt(sel);
		}
		catch(error){
			selection = -1;
		}
		if((selection > 0) && (selection < SELECTIONS.SIZE)){
			return true;
		}
		else{
			return false;
		}
	}
	//Print an error message
	private static printErrorMessage(): void{
		console.log("That is an invalid selection!");
	}
	//Handle what happens when a user wants to solve a problem
	private static async solveMenu(){
		let problemNumber: number = ProblemSelection.getProblemNumber();
		//This selection solves all problems in order
		if(problemNumber == 0){
			//Solve to every valid problem number, skipping over 0
			for(let problemLocation: number = 1;problemLocation < ProblemSelection.PROBLEM_NUMBERS.length;++problemLocation){
				//Solve the problems
				console.log(ProblemSelection.PROBLEM_NUMBERS[problemLocation] + ". ");
				ProblemSelection.solveProblem(ProblemSelection.PROBLEM_NUMBERS[problemLocation]);
			}
		}
		//This is if a single problem number was chosen
		else{
			//Solve the problem
			ProblemSelection.solveProblem(problemNumber);
		}
	}
	//Handle what happens when a user wants to see the description of a problem
	private static descriptionMenu(): void{
		//Give some extra space to print the description
		console.log("\n");

		//Get the problem number
		let problemNumber = ProblemSelection.getProblemNumber();

		//If the problem number is 0 print out all the descriptions
		if(problemNumber == 0){
			//Print the description for every valid problem number, skipping over 0
			for(let problemLocation: number = 1;problemLocation < ProblemSelection.PROBLEM_NUMBERS.length;++problemLocation){
				//Print the problem's description
				console.log(ProblemSelection.PROBLEM_NUMBERS[problemLocation] + ". ");
				ProblemSelection.printDescription(ProblemSelection.PROBLEM_NUMBERS[problemLocation]);
				console.log();
			}
		}
		//Otherwise print out a single problem's description
		else{
			ProblemSelection.printDescription(problemNumber);
		}
	}
}

//Run the program
Driver.main();
