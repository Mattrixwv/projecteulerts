//ProjectEulerTS/Problems/Problem10.ts
//Matthew Ellison
// Created: 03-24-21
//Modified: 07-14-21
//Find the sum of all the primes below two million
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { Problem } from "./Problem";
import { getSum } from "../../../Typescript/typescriptClasses/ArrayAlgorithms";
import { getPrimes } from "../../../Typescript/typescriptClasses/NumberAlgorithms";


export class Problem10 extends Problem{
	//Variables
	//Static variables
	private static GOAL_NUMBER = 2000000 - 1;	//The largest number to check for primes
	//Instance variables
	private sum: number;	//The sum of all of the prime numbers

	//Functions
	//Constructor
	public constructor(){
		super(`Find the sum of all the primes below ${Problem10.GOAL_NUMBER + 1}`);
		this.sum = 0;
	}
	//Operational functions
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Get the sum of all prime numbers < GOAL_NUMBER
		this.sum = getSum(getPrimes(Problem10.GOAL_NUMBER));


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the porblem is solved
		this.solved = true;
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.sum = 0;
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `The sum of all the primes < ${Problem10.GOAL_NUMBER + 1} is ${this.sum}`;
	}
	//Returns the sum that was requested
	public getSum(): number{
		this.solvedCheck("sum");
		return this.sum;
	}
}


/* Results:
The sum of all the primes < 2000000 is 142913828922
It took an average of 170.840 milliseconds to run this problem through 100 iterations
*/
