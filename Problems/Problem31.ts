//ProjectEulerTS/Problems/Problem31.ts
//Matthew Ellison
// Created: 05-28-21
//Modified: 07-14-21
//How many different ways can £2 be made using any number of coins?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { Problem } from "./Problem";


export class Problem31 extends Problem{
	//Variables
	//Static variables
	private static DESIRED_VALUE: number = 200;	//The value of voins we want
	//Instance variables
	private permutations: number;	//The number of permutations that are found

	//Functions
	//Constructor
	public constructor(){
		super("How many different ways can 2 pounds be made using any number of coins?");
		this.permutations = 0;
	}
	//Operational functions
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Start with 200p and remove the necessary coins with each loop
		for(let pound2 = Problem31.DESIRED_VALUE;pound2 >= 0;pound2 -= 200){
			for(let pound1 = pound2;pound1 >= 0;pound1 -= 100){
				for(let pence50 = pound1;pence50 >= 0;pence50 -= 50){
					for(let pence20 = pence50;pence20 >= 0;pence20 -= 20){
						for(let pence10 = pence20;pence10 >= 0;pence10 -= 10){
							for(let pence5 = pence10;pence5 >= 0;pence5 -= 5){
								for(let pence2 = pence5;pence2 >= 0;pence2 -= 2){
									++this.permutations;
								}
							}
						}
					}
				}
			}
		}


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		this.solved = true;
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.permutations = 0;
	}
	//Gets
	//Returns the result of solving the proble
	public getResult(): string{
		this.solvedCheck("result");
		return `There are ${this.permutations} ways to make 2 pounds with the given denominations of coins`;
	}
	//Returns the number of correct permutations of the coins
	public getPermutations(): number{
		this.solvedCheck("number of correct permutations of the coins");
		return this.permutations;
	}
}


/* Results:
There are 73682 ways to make 2 pounds with the given denominations of coins
It took an average of 197.293 microseconds to run this problem through 100 iterations
*/
