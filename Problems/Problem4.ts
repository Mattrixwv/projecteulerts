//ProjectEulerTS/Problems/Problem4.ts
//Matthew Ellison
// Created: 10-24-20
//Modified: 07-14-21
//Find the largest palindrome made from the product of two 3-digit numbers
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { Problem } from "./Problem";


export class Problem4 extends Problem{
	//Variables
	//Static variables
	private static START_NUM: number = 100;	//The first number to be multiplied
	private static END_NUM: number = 999;	//The last number to be multiplied
	//Instance variables
	private palindromes: number[];	//Holds all number that turn out to be palindromes

	//Constructor
	public constructor(){
		super("Find the largest palindrome made from the product of two 3-digit numbers");
	}
	//Operational functions
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Start at the first 3-digit number and check every one up to the last 3-digit number
		for(let firstNum: number = Problem4.START_NUM;firstNum <= Problem4.END_NUM;++firstNum){
			//You can start at the location of the first number because everything before that has already been tested. (100 * 101 == 101 * 100)
			for(let secondNum = firstNum;secondNum < Problem4.END_NUM;++secondNum){
				//Get the product
				let product: number = firstNum * secondNum;
				//Change the number into a string
				let productString: string = product.toString();
				//Reverse the string
				let reverseString: string = productString.split("").reverse().join("");

				//If the number and it's reverse are the same it is a palindrome so add it to the list
				if(productString == reverseString){
					this.palindromes.push(product);
				}
				//If it's not a palindrome ignore it and move to the next number
			}
		}

		//Sort the palindromes so that the last one is the largest
		this.palindromes = this.palindromes.sort((n1, n2) => n1 - n2);


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		this.solved = true;
	}

	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.palindromes = [];
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `The largest palindrome is ${this.palindromes[this.palindromes.length - 1]}`;
	}
	//Returns the list of all palindromes
	public getPalindromes(): number[]{
		this.solvedCheck("palindromes");
		return this.palindromes;
	}
	//Returns the largest palindrome
	public getLargestPalindrome(): number{
		this.solvedCheck("largest palindrome");
		return this.palindromes[this.palindromes.length - 1];
	}
}


/* Results:
The largest palindrome is 906609
It took an average of 121.130 milliseconds to run this problem through 100 iterations
*/
