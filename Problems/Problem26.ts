//ProjectEulerTS/Problems/Problem26.ts
//Matthew Ellison
// Created: 04-18-21
//Modified: 07-14-21
//Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { Problem } from "./Problem";


export class Problem26 extends Problem{
	//Variables
	//Static variables
	private static TOP_NUM: number = 999;	//The largest denominator to test
	//Instance variables
	private longestCycle: number;	//The length of the longest cycle
	private longestNumber: number;	//The starting denominator of the longest cycle

	//Functions
	//Constructor
	public constructor(){
		super(`Find the value of d <= ${Problem26.TOP_NUM} for which 1/d contains the longest recurring cycle in its decimal fraction part.`);
		this.longestCycle = 0;
		this.longestNumber = 1;
	}
	//Operational functions
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing nad end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Start with 1/2 and find out how long the logest cycle is by checking the reainders
		//Loop through every number from 2-999 and use it for the denominator
		for(let denominator: number = 2;denominator <= Problem26.TOP_NUM;++denominator){
			let denomList: number[] = [];
			let endFound: boolean = false;	//A flag for when we have found an end to the number (either a cycle or a 0 for remainder)
			let cycleFound: boolean = false;	//A flag to indicate a cycle was detected
			let numerator: number = 1;	//The numerator that will be divided. Always starts at 1
			while(!endFound){
				//Get the remainder after the division
				let remainder: number = numerator % denominator;
				//Check if the remainder is 0 and set the flag
				if(remainder == 0){
					endFound = true;
				}
				//Check if the remainder is in the list and set the appropriate flags
				else if(denomList.includes(remainder)){
					endFound = true;
					cycleFound = true;
				}
				//Else add it to the list
				else{
					denomList.push(remainder);
				}
				//Multiply the remainder by 10 to continue finding the next remainder
				numerator = remainder * 10;
			}
			//If a cycle was found check the size of the list against the largest cycle
			if(cycleFound){
				//If it is lager than the largest, set it as the new largest
				if(denomList.length > this.longestCycle){
					this.longestCycle = denomList.length;
					this.longestNumber = denominator;
				}
			}
		}


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		this.solved = true;
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.longestCycle = 0;
		this.longestNumber = 1;
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `The longest cycle is ${this.longestCycle} digits long\nIt started with the number ${this.longestNumber}`;
	}
	//Returns the length of the longest cycle
	public getLongestCycle(): number{
		this.solvedCheck("length of the longest cycle");
		return this.longestCycle;
	}
	//Returns the denominator that start the longest cycle
	public getLongestNumber(){
		this.solvedCheck("denominator that starts the longest cycle");
		return this.longestNumber;
	}
}


/* Results:
The longest cycle is 982 digits long
It started with the number 983
It took an average of 13.760 milliseconds to run this problem through 100 iterations
*/
