//ProjectEulerTS/Problems/Problem30.ts
//Matthew Ellison
// Created: 05-28-21
//Modified: 07-14-21
//Find the sum of all the numbers that can be written as the sum of the fifth powers of their digits.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { getSum } from "../../../Typescript/typescriptClasses/ArrayAlgorithms";
import { Problem } from "./Problem";


export class Problem30 extends Problem{
	//Variables
	//Static variables
	private static TOP_NUM: number = 1000000;	//This is the largest number that will be checked
	private static BOTTOM_NUM: number = 2;		//Start with 2 because 0 and 1 don't count
	private static POWER_RAISED: number = 5;	//This is the power that hte digits are raised to
	//Instance variables
	private sumOfFifthNumbers: number[];	//This is an array of the numbers thar are the sum of the fifth power of their digits
	private sum: number;	//This is the sum of the sumOfFifthNumbers array

	//Functions
	//Operational functions
	public constructor(){
		super(`Find the sum of all the numbers that can be written as the sum of the fifth powers of their digits.`);
		this.sumOfFifthNumbers = [];
	}
	//Operational functions
	//Returns an array with the individual digits of the number passed to it
	private getDigits(num: number): number[]{
		let listOfDigits: number[] = [];	//This array holds the individual digits of num
		//The easiest way to get the individual digits of a number is by converting it to a string
		let digits: string = num.toString();
		//Start with the first digit, convert it to an integer, store it in the array, and move to the next digit
		for(let cnt = 0;cnt < digits.length;++cnt){
			listOfDigits.push(parseInt(digits[cnt]));
		}
		//Return the list of digits
		return listOfDigits;
	}
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the functions
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Start with the lowest number and increment until you reach the largest number
		for(let currentNum = Problem30.BOTTOM_NUM;currentNum <= Problem30.TOP_NUM;++currentNum){
			//Get the digits of the number
			let digits: number[] = this.getDigits(currentNum);
			//Get the sum of the powers
			let sumOfPowers: number = 0;
			for(let num of digits){
				sumOfPowers += num ** Problem30.POWER_RAISED;
			}
			//Check if the sum of the powers is the same as the number
			//If it is add it to the list, otherwise continue to the next number
			if(sumOfPowers == currentNum){
				this.sumOfFifthNumbers.push(currentNum);
			}
		}

		//Get the sum of the numbers
		this.sum = getSum(this.sumOfFifthNumbers);


		//Stop the timer
		this.timer.stop();

		//THrow a flag to show the problem is solved
		this.solved = true;
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.sumOfFifthNumbers = [];
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `This sum of all the numbers that can be written as the sum of the fifth powers of their digits is ${this.sum}`;
	}
	//This returns the top number to be checked
	public static getTopNum(): number{
		return Problem30.TOP_NUM;
	}
	//This returns a copy of the vector holding all the numbers that are the sum of the fifth power of their digits
	public getListOfSumOfFifths(): number[]{
		this.solvedCheck("list of all numbers that are the sum of the 5th power of their digits");
		return this.sumOfFifthNumbers;
	}
	//This returns the sum of all entries in sumOfFifthNumbers
	public getSumOfList(): number{
		this.solvedCheck("sum of all numbers that are the sum of the 5th power of their digits");
		return this.sum;
	}
}


/* Results:
This sum of all the numbers that can be written as the sum of the fifth powers of their digits is 443839
It took an average of 180.976 milliseconds to run this problem through 100 iterations
*/
