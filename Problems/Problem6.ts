//ProjectEulerTS/Problems/Problem6.ts
//Matthew Ellison
// Created: 03-10-21
//Modified: 07-14-21
//Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { Problem } from "./Problem";


export class Problem6 extends Problem{
	//Variables
	//Static variables
	private static START_NUM: number = 1;	//The first number that needs to be counted
	private static END_NUM: number = 100;	//The last number that needs to be counted
	//Instance variables
	private sumOfSquares: number;	//Holds the sum of the squares of all the numbers
	private squareOfSum: number;	//Holds the square of the sum of all the numbers

	//Functions
	//Constructor
	public constructor(){
		super(`Find the difference between the sum of the squares and the square of the sum of the numbers ${Problem6.START_NUM}-${Problem6.END_NUM}`);
		this.sumOfSquares = 0;
		this.squareOfSum = 0;
	}
	//Operational functions
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Run through all numbers and add them to the appropriate sums
		for(let currentNum: number = Problem6.START_NUM;currentNum <= Problem6.END_NUM;++currentNum){
			this.sumOfSquares += (currentNum * currentNum);	//Square the number and add it to the variable
			this.squareOfSum += currentNum;	//Add the number to the sum to be squared later
		}
		//Square the sum of all the numbers
		this.squareOfSum *= this.squareOfSum;


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		this.solved = true;
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.squareOfSum = 0;
		this.sumOfSquares = 0;
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `The difference between the sum of the squares and the square of the sum of all numbers from ${Problem6.START_NUM}-${Problem6.END_NUM} is ${this.getDifference()}`;
	}
	//Returns the sum of all the squares
	public getSumOfSquares(): number{
		this.solvedCheck("sum of the squares");
		return this.sumOfSquares;
	}
	//Returns the square of all the sums
	public getSquareOfSum(){
		this.solvedCheck("square of the sums");
		return this.squareOfSum;
	}
	//Returns the requested difference
	public getDifference(): number{
		this.solvedCheck("difference between the two numbers");
		return Math.abs(this.sumOfSquares - this.squareOfSum);
	}
}


/* Results:
The difference between the sum of the squares and the square of the sum of all numbers from 1-100 is 25164150
It took an average of 344.001 nanoseconds to run this problem through 100 iterations
*/
