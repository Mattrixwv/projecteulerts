//ProjectEulerTS/Problems/Problem3.ts
//Matthew Ellison
// Created: 10-19-20
//Modified: 07-14-21
//The largest prime factor of 600851475143
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { Problem } from "./Problem";
import { getFactors } from "../../../Typescript/typescriptClasses/NumberAlgorithms";


export class Problem3 extends Problem{
	//Variables
	//Static variables
	private static GOAL_NUMBER: number = 600851475143;	//The number that needs factored
	//Instance variables
	private factors: number[];	//Holds the factors of goalNumber

	//Functions
	//Constructor
	public constructor(){
		super(`What is the largest prime factor of ${Problem3.GOAL_NUMBER}`);
		this.factors = [];
	}
	//Operational functions
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Get all the factors of the number
		this.factors = getFactors(Problem3.GOAL_NUMBER);
		//The last element should be the largest factor


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		this.solved = true;
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.factors = [];
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `The largest factor of the number ${Problem3.GOAL_NUMBER} is ${this.factors[this.factors.length - 1]}`;
	}
	//Returns the list of factors of the number
	public getFactors(): number[]{
		this.solvedCheck("factors");
		return this.factors;
	}
	//Returns the largest factor of the number
	public getLargestFactor(): number{
		this.solvedCheck("largest factor");
		return this.factors[this.factors.length];
	}
	//Returns the number for which we are getting the factor
	public getGoalNumber(): number{
		return Problem3.GOAL_NUMBER;
	}
}


/* Results:
The largest factor of the number 600851475143 is 6857
It took an average of 56.397 milliseconds to run this problem through 100 iterations
*/
