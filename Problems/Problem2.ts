//ProjectEulerTS/Problems/Problem2.ts
//Matthew Ellison
// Created: 10-19-20
//Modified: 07-14-21
//The sum of the even Fibonacci numbers less than 4,000,000
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { Problem } from "./Problem";
import { getAllFib } from "../../../Typescript/typescriptClasses/NumberAlgorithms";


export class Problem2 extends Problem{
	//Variables
	//Static variables
	private static TOP_NUM: number = 4000000 - 1;	//The largest number that will be checked as a fibonacci number
	//Instance variables
	private fullSum: number;	//Holds the sum of all the numbers

	//Functions
	//Constructor
	public constructor(){
		super(`What is the sum of the even Fibonacci numbers less than ${Problem2.TOP_NUM + 1}`);
		this.fullSum = 0;
	}
	//Operational functions
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Get a list of all fibonacci numbers <= TOP_NUM
		let fibNums = getAllFib(Problem2.TOP_NUM);
		//Setp through every element in the list checking ifit is even
		fibNums.forEach(num => {
			//If the number is even add it to the running tally
			if((num % 2) == 0){
				this.fullSum += num;
			}
		});


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		this.solved = true;
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.fullSum = 0;
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `The sum of all even fibonacci numbers <= ${Problem2.TOP_NUM} is ${this.fullSum}`;
	}
	//Returns the requested sum
	public getSum(): number{
		this.solvedCheck("sum");
		return this.fullSum;
	}
}


/* Results:
The sum of all even fibonacci numbers <= 3999999 is 4613732
It took an average of 6.330 microseconds to run this problem through 100 iterations
*/
