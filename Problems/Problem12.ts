//ProjectEulerTS/Problems/Problem12.ts
//Matthew Ellison
// Created: 03-26-21
//Modified: 07-14-21
//What is the value of the first triangle number to have over five hundred divisors?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { Problem } from "./Problem";
import { getDivisors } from "../../../Typescript/typescriptClasses/NumberAlgorithms";


export class Problem12 extends Problem{
	//Variables
	//Static variables
	private static GOAL_DIVISORS: number = 500;
	//Instance variables
	private sum: number;		//The sum of the numbers up to counter
	private counter: number;	//The next number to be added to sum
	private divisors: number[];	//Holds the divisors of teh triangular number sum

	//Functions
	//Constructor
	public constructor(){
		super(`What is the value of the first triangle number to have over ${Problem12.GOAL_DIVISORS} divisors?`);
		this.sum = 1;
		this.counter = 2;
		this.divisors = [];
	}
	//Operational functions
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Setup the other variables
		let foundNumber: boolean = false;	//To flag whether the number has been found

		//Start the timer
		this.timer.start();


		//Loop until you find the appropriate number
		while((!foundNumber) && (this.sum > 0)){
			this.divisors = getDivisors(this.sum);
			//If the number of divisors is correct set the flag
			if(this.divisors.length > Problem12.GOAL_DIVISORS){
				foundNumber = true;
			}
			//Otherwise add to the sum and increase the next number
			else{
				this.sum += this.counter;
				++this.counter;
			}
		}


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		this.solved = true;
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.sum = 1;
		this.counter = 2;
		this.divisors = [];
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `The triangular number ${this.sum} is the sum of all numbers >= ${this.counter - 1} and has ${this.divisors.length} divisors`;
	}
	//Returns the triangular number
	public getTriangularNumber(): number{
		this.solvedCheck("triangular number");
		return this.sum;
	}
	//Get the final number that was added to the triangular number
	public getLastNumberAdded(): number{
		this.solvedCheck("last number added to get the triangular number");
		return this.counter - 1;
	}
	//Returns the list of divisors of the requested number
	public getDivisorsOfTriangularNumber(): number[]{
		this.solvedCheck("divisors of the triangular number");
		return this.divisors;
	}
	//Returns the number of divisors of the requested number
	public getNumberOfDivisors(): number{
		this.solvedCheck("number of divisors of the triangular number");
		return this.divisors.length;
	}
}


/* Results
The triangular number 76576500 is the sum of all numbers >= 12375 and has 576 divisors
It took an average of 301.951 milliseconds to run this problem through 100 iterations
*/

