//ProjectEulerTS/Problems/Problem21.ts
//Matthew Ellison
// Created: 04-08-21
//Modified: 07-14-21
//Evaluate the sum of all the amicable numbers under 10000
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { getSum } from "../../../Typescript/typescriptClasses/ArrayAlgorithms";
import { getDivisors } from "../../../Typescript/typescriptClasses/NumberAlgorithms";
import { Problem } from "./Problem";


export class Problem21 extends Problem{
	//Variables
	//Static variables
	private static LIMIT: number = 10000;	//The number that is > the largest number to be checked
	//Instance variables
	private divisorSum: number[];	//Holds the sum of the divisors of the subscript number
	private amicable: number[];	//Holds all amicable numbers

	//Functions
	//Constructor
	public constructor(){
		super(`Evaluate the sum of all the amicable numbers under ${Problem21.LIMIT}`);
		this.divisorSum = [];
		this.amicable = [];
		this.reserveArray();
	}
	//Operational functions
	//Reserve the size of the array to speed up insertion
	private reserveArray(): void{
		//Reserving it now makes it faster later
		//Make sure the array is filled with 0's
		while(this.divisorSum.length < Problem21.LIMIT){
			this.divisorSum.push(0);
		}
	}
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Generate the divisors of all numbers < 10000, get their sum, and add it to the list
		for(let cnt = 1;cnt < Problem21.LIMIT;++cnt){
			let divisors: number[] = getDivisors(cnt);	//Get all the divisors of a number
			if(divisors.length > 1){
				divisors.pop();	//Remove the last element because it will be the number itself
			}
			this.divisorSum[cnt] = getSum(divisors);
		}
		//Check every sum of divisors in the list for matching sum
		for(let cnt = 1;cnt < this.divisorSum.length;++cnt){
			let sum: number = this.divisorSum[cnt];
			//If the sum is greater than the number of divisors then it is impossible to be amicable. Skip the number and continue
			if(sum >= this.divisorSum.length){
				continue;
			}
			//We know that divisorSum[cnt] == sum, so if divisorSum[sum] == cnt we found an amicable number
			if(this.divisorSum[sum] == cnt){
				//A number can't be amicable with itself
				if(sum == cnt){
					continue;
				}
				//Add it to the array of amicable numbers
				this.amicable.push(cnt);
			}
		}

		//Sort the array for neatness
		this.amicable.sort((n1, n2) => n1 - n2);


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		this.solved = true;
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.divisorSum = [];
		this.amicable = [];
		this.reserveArray();
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		let result: string = `All amicable numbers less than ${Problem21.LIMIT} are\n`;
		for(let cnt = 0;cnt < this.amicable.length;++cnt){
			result += `${this.amicable[cnt]}\n`;
		}
		result += `The sum of all of these amicable numbers is ${getSum(this.amicable)}`;

		return result;
	}
	//Returns a vector with all of the amicable numbers calculated
	public getAmicable(): number[]{
		this.solvedCheck("amicable numbers");
		return this.amicable;
	}
	//Returns the sum of all of the amicable numbers
	public getSum(): number{
		this.solvedCheck("sum of the amicable numbers");
		return getSum(this.amicable);
	}
}


/* Results:
The sum of all of these amicable numbers is 31626
It took an average of 7.454 milliseconds to run this problem through 100 iterations
*/
