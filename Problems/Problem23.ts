//ProjectEulerTS/Problems/Problem23.ts
//Matthew Ellison
// Created: 04-08-21
//Modified: 07-14-21
//Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { getSum } from "../../../Typescript/typescriptClasses/ArrayAlgorithms";
import { getDivisors } from "../../../Typescript/typescriptClasses/NumberAlgorithms";
import { Problem } from "./Problem";


export class Problem23 extends Problem{
	//Variables
	//Static variables
	private static MAX_NUM: number = 28123;	//The largest number to be checked
	//Instance variables
	private divisorSums: number[];	//This gives the sum of the divisors at subscripts
	private sum: number;	//The sum of all the numbers we are looking for

	//Functions
	//Constructor
	public constructor(){
		super(`Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers`);
		this.divisorSums = [];
		this.reserveArray();
		this.sum = 0;
	}
	//Operational functions
	//Reserve the size of the array to speed up insertion
	private reserveArray(): void{
		//It is faster to reserve the appropriate amount of ram now
		//Make sure every element has a 0 in it's location
		while(this.divisorSums.length <= Problem23.MAX_NUM){
			this.divisorSums.push(0);
		}
	}
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();

		//Get the sum of the divisors of all numbers < MAX_NUM
		for(let cnt = 1;cnt < Problem23.MAX_NUM;++cnt){
			let div: number[] = getDivisors(cnt);
			//Remove the last element, which is the number itself. This gives us the propper divisors
			if(div.length > 1){
				div.pop();
			}
			this.divisorSums[cnt] = getSum(div);
		}

		//Get the abundant numbers
		let abund: number[] = [];
		for(let cnt = 0;cnt < this.divisorSums.length;++cnt){
			if(this.divisorSums[cnt] > cnt){
				abund.push(cnt);
			}
		}

		//Check if each number can be the sum of 2 abundant numbers and add to the sum if no
		for(let cnt = 1;cnt < Problem23.MAX_NUM;++cnt){
			if(!this.isSum(abund, cnt)){
				this.sum += cnt;
			}
		}

		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		this.solved = true;
	}
	//A function that returns true if num can be created by adding two elements from abund and false if it cannot
	private isSum(abund: number[], num: number): boolean{
		let sum: number = 0;
		//Pick a number for the first part of the sum
		for(let firstNum = 0;firstNum < abund.length;++firstNum){
			//Pick a number for the second part of the sum
			for(let secondNum = firstNum;secondNum < abund.length;++secondNum){
				sum = abund[firstNum] + abund[secondNum];
				if(sum == num){
					return true;
				}
				else if(sum > num){
					break;
				}
			}
		}
		//If you have run through the entire list and did not find a sum then it is false
		return false;
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.divisorSums = [];
		this.reserveArray();
		this.sum = 0;
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `The answer is ${this.sum}`;
	}
	//Returns the sum of the numbers asked for
	public getSum(): number{
		this.solvedCheck("sum");
		return this.sum;
	}
}


/* Results:
The answer is 4179871
It took an average of 7.798 seconds to run this problem through 100 iterations
*/
