//ProjectEulerTS/Problems/Problem34.ts
//Matthew Ellison
// Created: 06-01-21
//Modified: 07-14-21
//Find the sum of all numbers which are equal to the sum of the factorial of their digits
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { factorial } from "../../../Typescript/typescriptClasses/NumberAlgorithms";
import { Problem } from "./Problem";


export class Problem34 extends Problem{
	//Variables
	//Static variables
	private static MAX_NUM: number = 1499999;	//The largest num that can be the sum of its own digits
	//Instance variables
	private factorials: number[];	//Holds the pre-computed factorials of the numbers 0-9
	private sum: number;	//Holds the sum of all numbers equal to the sum of their digit's factorials

	//Functions
	//Constructor
	public constructor(){
		super("Find the sum of all numbers which are equal to the sum of the factorial of their digits");
		this.sum = 0;
		this.factorials = [];
		for(let cnt = 0;cnt <= 9;++cnt){
			this.factorials.push(0);
		}
	}
	//Operational functions
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Pre-compute the possible factorials form 0! to 9!
		for(let cnt = 0;cnt <= 9;++cnt){
			this.factorials[cnt] = factorial(cnt);
		}
		//Run through all possible numbers from 3-MAX_NUM and see if they equal the sum of their digit's factorials
		for(let cnt = 3;cnt < Problem34.MAX_NUM;++cnt){
			//Split the number into tis digits and add each one to the sum
			let numString: string = cnt.toString();
			let currentSum: number = 0;
			for(let numCnt = 0;numCnt < numString.length;++numCnt){
				let digit = numString.charAt(numCnt);
				currentSum += this.factorials[parseInt(digit)];
			}
			//If the number is equal to the sum add the sum to the running sum
			if(currentSum == cnt){
				this.sum += currentSum;
			}
		}


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		this.solved = true;
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.sum = 0;
		this.factorials = [];
		for(let cnt = 0;cnt <= 9;++cnt){
			this.factorials.push(0);
		}
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `The sum of all numbers that are the sum of their digit's factorials is ${this.sum}`;
	}
	//Returns the list of factorials from 0-9
	public getFactorials(): number[]{
		this.solvedCheck("list of factorials");
		return this.factorials;
	}
	//Returns the sum of all numbers equal to the sum of their digit's factorials
	public getSum(): number{
		this.solvedCheck("sum");
		return this.sum;
	}
}


/* Results:
The sum of all numbers that are the sum of their digit's factorials is 40730
It took an average of 87.615 milliseconds to run this problem through 100 iterations
*/
