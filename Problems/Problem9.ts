//ProjectEulerTS/Problems/Problem9.ts
//Matthew Ellison
// Created: 03-24-21
//Modified: 07-14-21
//There exists exactly one Pythagorean triplet for which a + b + c = 1000. Find the product abc.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { Unsolved } from "../Unsolved";
import { Problem } from "./Problem";


export class Problem9 extends Problem{
	//Variables
	//Static variables
	private static GOAL_SUM: number = 1000;	//The number that we want the sum of a, b, and c to equal
	//Instance variables
	private a: number;		//The size of the first side
	private b: number;		//The size of the second side
	private c: number;		//The size of the hyp
	private found: boolean;	//A flag to determine if we have found the solution yet

	//Functions
	//Constructor
	public constructor(){
		super(`There exists exactly one Pythagorean triplet for which a + b + c = ${Problem9.GOAL_SUM}. Find the product abc.`);
		this.a = 1;
		this.b = 0;
		this.c = 0;
		this.found = false;
	}
	//Operational functions
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Loop through all possible a's
		while((this.a < Problem9.GOAL_SUM) && (!this.found)){
			this.b = this.a + 1;	//b must be larger than a
			this.c = Math.hypot(this.a, this.b);	//Compute the hyp

			//Loop through all possible b's for this a
			while((this.a + this.b + this.c) < Problem9.GOAL_SUM){
				++this.b;
				this.c = Math.hypot(this.a, this.b);
			}

			//If the sum == 1000 you found the number, otherwise go to the next possible a
			if((this.a + this.b + this.c) == Problem9.GOAL_SUM){
				this.found = true;
			}
			else{
				++this.a;
			}
		}


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		if(this.found){
			this.solved = true;
		}
		else{
			throw new Unsolved("The problem was not solved!");
		}
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.a = 1;
		this.b = 0;
		this.c = 0;
		this.found = false;
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `The Pythagorean triplet is ${this.a} + ${this.b} + ${this.c}\nThe numbers' product is ${this.getProduct()}`;
	}
	//Returns the length of the first side
	public getSideA(): number{
		this.solvedCheck("first side");
		return this.a;
	}
	//Returns the length of the second side
	public getSideB(): number{
		this.solvedCheck("second side");
		return this.b;
	}
	//Returns the length of the hyp
	public getSideC(): number{
		this.solvedCheck("third side");
		return this.c;
	}
	//Returns the product of the 3 sides
	public getProduct(): number{
		this.solvedCheck("product of all three sides");
		return this.a * this.b * this.c;
	}
}


/* Results:
The Pythagorean triplet is 200 + 375 + 425
The numbers' product is 31875000
It took an average of 2.191 milliseconds to run this problem through 100 iterations
*/
