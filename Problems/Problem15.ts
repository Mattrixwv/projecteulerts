//ProjectEulerTS/Problems/Problem15.ts
//Matthew Ellison
// Created: 03-29-21
//Modified: 07-14-21
//How many routes from the top left corner to the bottom right corner are there through a 20×20 grid if you can only move right and down?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { Problem } from "./Problem";


export class Problem15 extends Problem{
	//Variables
	//Static variables
	private static WIDTH: number = 20;	//The width of the box to traverse
	private static LENGTH: number = 20;	//The height of the box to traverse
	//Instance variables
	private numOfRoutes: number;	//The number of routes from 0, 0 to 20, 20

	//Functions
	//Constructor
	public constructor(){
		super(`How many routes from the top left corner to the bottom right corner are there through a ${Problem15.WIDTH}x${Problem15.LENGTH} grid if you can only move right and down?`);
		this.numOfRoutes = 0;
	}
	//Operational functions
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//We write this as a recursive function
		//When in a location it always move right first, then down
		this.move(0, 0);


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		this.solved = true;
	}
	//This function acts as a handler for moving the position on the grid and counting the disctance
	//It moves right first, then down
	private move(currentX: number, currentY: number): void{
		//Check if you are at the end and act accordingly
		if((currentX == Problem15.WIDTH) && (currentY == Problem15.LENGTH)){
			++this.numOfRoutes;
			return;
		}

		//Move right if possible
		if(currentX < Problem15.WIDTH){
			this.move(currentX + 1, currentY);
		}

		//Move down if possible
		if(currentY < Problem15.LENGTH){
			this.move(currentX, currentY + 1);
		}
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.numOfRoutes = 0;
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `The number of routes is ${this.numOfRoutes}`;
	}
	//Returns the number of routes found
	public getNumberOfRoutes(): number{
		this.solvedCheck("number of routes");
		return this.numOfRoutes;
	}
}


/* Results:
The number of routes is 137846528820
It took 37.058 minutes to solve this problem.
*/
