//ProjectEulerTS/Problems/Problem32.ts
//Matthew Ellison
// Created: 05-28-21
//Modified: 07-14-21
//Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { findNumOccurrence } from "../../../Typescript/typescriptClasses/StringAlgorithms";
import { Problem } from "./Problem";


//Structures
//Holds the set of numbers that make a product
class ProductSet{
	private multiplicand: number;
	private multiplier: number;
	public constructor(multiplicand: number, multiplier: number){
		this.multiplicand = multiplicand;
		this.multiplier = multiplier;
	}
	public getMultiplicand(): number{
		return this.multiplicand;
	}
	public getMultiplier(): number{
		return this.multiplier;
	}
	public getProduct(): number{
		return (this.multiplicand * this.multiplier);
	}
	public equals(secondSet: ProductSet): boolean{
		//If an object is compared to itself return true
		if(secondSet == this){
			return true;
		}
		//Return true if the products are the same
		return (this.getProduct() == secondSet.getProduct());
	}
	public toString(): string{
		return `${this.multiplicand}${this.multiplier}${this.getProduct()}`;
	}
}

export class Problem32 extends Problem{
	//Variables
	//Static variables
	private static TOP_MULTIPLICAND: number = 99;	//The largest multiplicand to check
	private static TOP_MUTLIPLIER: number = 4999;	//The larest multiplier to check
	//Instance variables
	private listOfProducts: ProductSet[];	//The list of unique products that are 1-9 pandigital
	private sumOfPandigitals: number;	//The sum of the products of the pandigital numbers

	//Functions
	//Constructor
	public constructor(){
		super("Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.");
		this.listOfProducts = [];
		this.sumOfPandigitals = 0;
	}
	//Operational functions
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Create the multiplicand and start working your way up
		for(let multiplicand = 1;multiplicand <= Problem32.TOP_MULTIPLICAND;++multiplicand){
			//Run through all possible multipliers
			for(let multiplier = multiplicand;multiplier <= Problem32.TOP_MUTLIPLIER;++multiplier){
				let currentProductSet: ProductSet = new ProductSet(multiplicand, multiplier);
				//If the product is too long move on to the next possible number
				if(currentProductSet.toString().length > 9){
					break;
				}
				//If the current number is a pandigital that doesn't already exist in the list add it to the list
				if(this.isPandigital(currentProductSet)){
					if(!this.listOfProducts.some(prod => prod.getProduct() == currentProductSet.getProduct())){
						this.listOfProducts.push(currentProductSet);
					}
				}
			}
		}

		//Get the sum of the products of the pandigitals
		for(let prod of this.listOfProducts){
			this.sumOfPandigitals += prod.getProduct();
		}


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		this.solved = true;
	}
	//Returns true if the passed productset is 1-9 pandigital
	private isPandigital(currentSet: ProductSet): boolean{
		//Get the numbers out of the object and put them into a string
		let numberString: string = currentSet.toString();
		//Make sure the string is the currect length
		if(numberString.length != 9){
			return false;
		}
		//Make sure every number from 1-9 is contains exactly once
		for(let panNumber: number = 1;panNumber <= 9;++panNumber){
			//Make sure there is exactly one of this number contained in the string
			if(findNumOccurrence(numberString, panNumber.toString()) != 1){
				return false;
			}
		}
		//If all numbers were found in the string return true
		return true;
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.listOfProducts = [];
		this.sumOfPandigitals = 0;
	}
	//Gets
	public getResult(): string{
		this.solvedCheck("result");
		return `There are ${this.listOfProducts.length} unique 1-9 pandigitals\nThe sum of the products of the pandigitals is ${this.sumOfPandigitals}`
	}
	//Returns the sum of pandigitals
	public getSumOfPandigitlas(): number{
		this.solvedCheck("sum of the pandigitals");
		return this.sumOfPandigitals;
	}
}


/* Results:
There are 7 unique 1-9 pandigitals
The sum of the products of the pandigitals is 45228
It took an average of 5.049 milliseconds to run this problem through 100 iterations
*/
