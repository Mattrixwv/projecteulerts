//ProjectEulerTS/Problems/Problem8.ts
//Matthew Ellison
// Created: 03-10-21
//Modified: 07-14-21
//Find the thirteen adjacent digits in the 1000-digit number that have the greatest product. What is the value of this product?
/*
73167176531330624919225119674426574742355349194934
96983520312774506326239578318016984801869478851843
85861560789112949495459501737958331952853208805511
12540698747158523863050715693290963295227443043557
66896648950445244523161731856403098711121722383113
62229893423380308135336276614282806444486645238749
30358907296290491560440772390713810515859307960866
70172427121883998797908792274921901699720888093776
65727333001053367881220235421809751254540594752243
52584907711670556013604839586446706324415722155397
53697817977846174064955149290862569321978468622482
83972241375657056057490261407972968652414535100474
82166370484403199890008895243450658541227588666881
16427171479924442928230863465674813919123162824586
17866458359124566529476545682848912883142607690042
24219022671055626321111109370544217506941658960408
07198403850962455444362981230987879927244284909188
84580156166097919133875499200524063689912560717606
05886116467109405077541002256983155200055935729725
71636269561882670428252483600823257530420752963450
*/
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { Problem } from "./Problem";


export class Problem8 extends Problem{
	//Variables
	//Static variables
	private static NUMBER: string = "7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450";
	//Instance variables
	private maxNums: string;	//Holds the string of the largest product
	private maxProduct: number;	//Holds the largest product of 13 numbers

	//Functions
	//Constructor
	public constructor(){
		super("Find the thirteen adjacent digits in the 1000-digit number that have the greatest product. What is the value of this product?");
		this.maxNums = "";
		this.maxProduct = 0;
	}
	//Operational functions
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Cycle through the string of numbers looking for the maximum product
		for(let cnt: number = 12;cnt < Problem8.NUMBER.length;++cnt){
			let currentProduct: number = parseInt(Problem8.NUMBER.substring(cnt - 12, cnt - 11)) * parseInt(Problem8.NUMBER.substring(cnt - 11, cnt - 10)) * parseInt(Problem8.NUMBER.substring(cnt - 10, cnt - 9)) * parseInt(Problem8.NUMBER.substring(cnt - 9, cnt - 8)) * parseInt(Problem8.NUMBER.substring(cnt - 8, cnt - 7)) * parseInt(Problem8.NUMBER.substring(cnt - 7, cnt - 6)) * parseInt(Problem8.NUMBER.substring(cnt - 6, cnt - 5)) * parseInt(Problem8.NUMBER.substring(cnt - 5, cnt - 4)) * parseInt(Problem8.NUMBER.substring(cnt - 4, cnt - 3)) * parseInt(Problem8.NUMBER.substring(cnt - 3, cnt - 2)) * parseInt(Problem8.NUMBER.substring(cnt - 2, cnt - 1)) * parseInt(Problem8.NUMBER.substring(cnt - 1, cnt)) * parseInt(Problem8.NUMBER.substring(cnt, cnt + 1));

			//Check if the product is greater than the current maximum
			if(currentProduct > this.maxProduct){
				this.maxNums = Problem8.NUMBER.substring(cnt - 12, cnt + 1);
				this.maxProduct = currentProduct;
			}
		}


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		this.solved = true;
	}
	//Reset the porblem so it can be run again
	public reset(): void{
		super.reset();
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `The greatest product is ${this.maxProduct}\nThe numbers are ${this.maxNums}`;
	}
	//Returns the string of numbers that produces the largest product
	public getLargestNums(): string{
		this.solvedCheck("numbers that make the largest product");
		return this.maxNums;
	}
	//Returns the requested product
	public getLargestProduct(): number{
		this.solvedCheck("product of the numbers");
		return this.maxProduct;
	}
}


/* Results:
The greatest product is 23514624000
The numbers are 5576689664895
It took an average of 121.325 microseconds to run this problem through 100 iterations
*/
