//ProjectEulerTS/Problems/Problem5.ts
//Matthew Ellison
// Created: 10-26-20
//Modified: 07-14-21
//What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { Problem } from "./Problem";


export class Problem5 extends Problem{
	//Variables
	//Instance variables
	private smallestNum: number;	//The smallest number that is found

	//Functions
	//Constructor
	public constructor(){
		super("What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?");
		this.smallestNum = 0;
	}
	//Operational functions
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Start at 20 because it must at least be divisible by 20. Increment by 2 because it must be an even number to be divisible by 2
		let numFound: boolean = false;	//A flag for finding the divisible number
		let currentNum: number = 20;	//The number that it is currently checking against
		while((currentNum > 0) && (!numFound)){
			//Start by assuming you found the number (because we throw a flag if we didn't find it)
			numFound = true;
			//Step through every number from 1-20 seeing if the current number is divisible by it
			for(let divisor: number = 1;divisor <= 20;++divisor){
				//If it is not divisible then throw a flag and start looking at the next number
				if((currentNum % divisor) != 0){
					numFound = false;
					break;
				}
			}
			//If you didn't find the correct number then increment by 2
			if(!numFound){
				currentNum += 2;
			}
		}
		//Save the current number as the smallest
		this.smallestNum = currentNum


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		this.solved = true;
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.smallestNum = 0;
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `The smallest positive number evenly divisible by all number 1-20 is ${this.smallestNum}`;
	}
	//Returns the requested number
	public getNumber(): number{
		this.solvedCheck("number");
		return this.smallestNum;
	}
}


/* Results:
The smallest positive number evenly divisible by all number 1-20 is 232792560
It took an average of 892.378 milliseconds to run this problem through 100 iterations
*/
