//ProjectEulerTS/Problems/Problem1.ts
//Matthew Ellison
// Created: 10-18-20
//Modified: 07-14-21
//What is the sum of all the multiples of 3 or 5 that are less than 1000
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { Unsolved } from "../Unsolved";
import {Problem} from "./Problem";


export class Problem1 extends Problem{
	//Variables
	//Static variables
	private static TOP_NUM = 999;	//THe largest number to be checked
	//Instance variables
	private fullSum: number = 0;	//The sum of all the numbers

	//Functions
	//Constructor
	public constructor(){
		super(`What is the sum of all the multiples of 3 or 5 that are less than ${Problem1.TOP_NUM + 1}?`);
	}
	//Operational functions
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Get the sum of the progressions of 3 and 5 and remove the sum of progressions of the overlap
		this.fullSum = this.sumOfProgression(3) + this.sumOfProgression(5) - this.sumOfProgression(3 * 5);


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		this.solved = true;
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.fullSum = 0;
	}
	//Gets the sum of the progression of the multiple
	private sumOfProgression(multiple: number): number{
		let numTerms = Math.floor(Problem1.TOP_NUM / multiple);	//Get the sum of the progressions of 3 and 5 and remove the sum of progressions of the overlap
		//The sum of progression formula is (n / 2)(a + l). n = number of terms, a = multiple, l = last term
		return ((numTerms / 2) * (multiple + (numTerms * multiple)))
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `The sum of all numbers < ${Problem1.TOP_NUM + 1} is ${this.fullSum}`;
	}
	//Returns the requested sum
	public getSum(): number{
		this.solvedCheck("sum");
		return this.fullSum;
	}
}


/* Results:
The sum of all numbers < 1000 is 233168
It took an average of 1.921 microseconds to run this problem through 100 iterations
*/
