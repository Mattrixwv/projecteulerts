//ProjectEulerTS/Problems/Problem.ts
//Matthew Ellison
// Created: 10-18-20
//Modified: 07-14-21
//What is the sum of all the multiples of 3 or 5 that are less than 1000
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { Stopwatch } from "../../../Typescript/typescriptClasses/Stopwatch";
import { Unsolved } from "../Unsolved";


export abstract class Problem{
	//Variables
	private description: string;
	protected timer: Stopwatch = new Stopwatch();
	protected solved: boolean;

	//Constructor
	public constructor(description: string){
		this.description = description;
		this.solved = false;
	}
	//Make sure the problem has been solved and throw an exception if not
	protected solvedCheck(str: string): void{
		if(!this.solved){
			throw new Unsolved("You must solve the problem before you can see the " + str);
		}
	}
	//Gets
	public getDescription(): string{
		return this.description;
	}
	//Returns the result of solving the problem
	public abstract getResult(): string;
	//Returns the time taken to run the problem as a string
	public getTime(): string{
		this.solvedCheck("time it took to run the algorithm");
		return this.timer.getStr();
	}
	//Returns the timer as a stopwatch
	public getTimer(): Stopwatch{
		this.solvedCheck("timer");
		return this.timer;
	}
	//Solve the problem
	public abstract solve(): void;
	//Reset the problem so it can be run again
	public reset(): void{
		this.timer.reset();
		this.solved = false;
	}
}
