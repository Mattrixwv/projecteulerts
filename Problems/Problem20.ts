//ProjectEulerTS/Problems/Problem20.ts
//Matthew Ellison
// Created: 04-07-21
//Modified: 07-14-21
//What is the sum of the digits of 100!?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { Problem } from "./Problem";


export class Problem20 extends Problem{
	//Variables
	//Static variables
	private static TOP_NUM: number = 100;	//The largest number that will be multiplied
	//Instance variables
	private num: bigint;	//Holds the number 100!
	private sum: number;	//The sum of the digits of num

	//Functions
	//Constructor
	public constructor(){
		super(`What is the sum of the digits of ${Problem20.TOP_NUM}!?`);
		this.num = 1n;
		this.sum = 0;
	}
	//Operational functions
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Run through every number from 1 to 100 and multiply it by the current num to generate 100!
		for(let cnt = Problem20.TOP_NUM;cnt > 1;--cnt){
			this.num *= BigInt(cnt);
		}

		//Get a string of the number because it is easier to pull aprat the individual characters
		let numString: string = String(this.num);
		//Run through every character in the string, convert it back to an integer and add it to the running sum
		for(let cnt = 0;cnt < numString.length;++cnt){
			let digit = numString.charAt(cnt);
			this.sum += parseInt(digit);
		}


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		this.solved = true;
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.num = 1n;
		this.sum = 0;
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `${Problem20.TOP_NUM}! = ${this.num}\nThe sum of the digits is: ${this.sum}`;
	}
	//Returns the number 100!
	public getNumber(): bigint{
		this.solvedCheck("number");
		return this.num;
	}
	//Returns the number 100! in a string
	public getNumberString(): string{
		this.solvedCheck("number as a string");
		return this.num.toString();
	}
	//Returns the sum of the digits of 100!
	public getSum(): number{
		this.solvedCheck("sum of the digits");
		return this.sum;
	}
}


/* Results:
100! = 93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000
The sum of the digits is: 648
It took an average of 84.320 microseconds to run this problem through 100 iterations
*/
