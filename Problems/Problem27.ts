//ProjectEulerTS/Problems/Problem26.ts
//Matthew Ellison
// Created: 05-24-21
//Modified: 07-14-21
//Find the product of the coefficients, |a| < 1000 and |b| <= 1000, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n=0.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { isPrime } from "../../../Typescript/typescriptClasses/NumberAlgorithms";
import { Problem } from "./Problem";


export class Problem27 extends Problem{
	//Variables
	//Static variables
	private static LARGEST_POSSIBLE_A: number = 999;
	private static LARGEST_POSSIBLE_B: number = 1000;
	//Instance variables
	private topA: number;	//The A for the most n's generated
	private topB: number;	//The B for the most n's generated
	private topN: number;	//The most n's generated

	//Functions
	//Constructor
	public constructor(){
		super(`Find the product of the coefficients, |a| <= ${Problem27.LARGEST_POSSIBLE_A} and |b| <= ${Problem27.LARGEST_POSSIBLE_B}, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n=0`);
		this.topA = 0;
		this.topB = 0;
		this.topN = 0;
	}
	//Operational functions
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Start with the lowest possible A and check all possibilities after that
		for(let a: number = -Problem27.LARGEST_POSSIBLE_A; a <= Problem27.LARGEST_POSSIBLE_A;++a){
			//Start witht he lowest possible B and check all possibilities after that
			for(let b: number = -Problem27.LARGEST_POSSIBLE_B;b <= Problem27.LARGEST_POSSIBLE_B;++b){
				//Start with n=0 and check the formula to see how many primes you can get the concecutive n's
				let n: number = 0;
				let quadratic: number = (n * n) + (a * n) + b;
				while(isPrime(quadratic)){
					++n;
					quadratic = (n * n) + (a * n) + b;
				}
				--n;	//Negate an n because the last formula failed

				//Set all the largest number if this created more primes than any other
				if(n > this.topN){
					this.topN = n;
					this.topB = b;
					this.topA = a;
				}
			}
		}


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem solved
		this.solved = true;
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.topA = 0;
		this.topB = 0;
		this.topN = 0;
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `The greatest number of primes found is ${this.topN}\nIt was found with A = ${this.topA}, B = ${this.topB}\nThe product of A and B is ${this.getProduct()}`;
	}
	//Returns the top A that was generated
	public getTopA(): number{
		this.solvedCheck("largest A");
		return this.topA;
	}
	//Returns the top B that was generated
	public getTopB(): number{
		this.solvedCheck("largest B");
		return this.topB;
	}
	//Returns the top N that was generated
	public getTopN(): number{
		this.solvedCheck("largest N");
		return this.topN;
	}
	//Returns the product of A and B for the answer
	public getProduct(): number{
		this.solvedCheck("product of A and B");
		return this.topA * this.topB;
	}
}


/* Results:
The greatest number of primes found is 70
It was found with A = -61, B = 971
The product of A and B is -59231
It took an average of 50.251 milliseconds to run this problem through 100 iterations
*/
