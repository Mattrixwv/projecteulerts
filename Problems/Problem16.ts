//ProjectEulerTS/Problems/Problem16.ts
//Matthew Ellison
// Created: 03-29-21
//Modified: 07-14-21
//What is the sum of the digits of the number 2^1000?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { Problem } from "./Problem";


export class Problem16 extends Problem{
	//Variables
	//Static variables
	private static NUM_TO_POWER: bigint = 2n;	//The number that is going to be raised to a power
	private static POWER: bigint = 1000n;		//The power that the number is going to be raised to
	//Instance variables
	private num: bigint;	//The number to be calculated
	private sumOfElements: number;	//The sum of all digits in the number

	//Functions
	//Construcotr
	public constructor(){
		super(`What is the sum of the digits of the number ${Problem16.NUM_TO_POWER}^${Problem16.POWER}`);
		this.num = 0n;
		this.sumOfElements = 0;
	}
	//Operational functions
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Get the number
		this.num = Problem16.NUM_TO_POWER ** Problem16.POWER;

		//Get a string of the number
		let numString: string = this.num.toString();

		//Add up the individual characters of the string
		for(let cnt: number = 0;cnt < numString.length;++cnt){
			this.sumOfElements += parseInt(numString.charAt(cnt));
		}


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the porblem is solved
		this.solved = true;
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.num = 0n;
		this.sumOfElements = 0;
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `${Problem16.NUM_TO_POWER}^${Problem16.POWER} = ${this.num}\nThe sum of the elements is ${this.sumOfElements}`;
	}
	//Returns the number that was calculated
	public getNumber(): bigint{
		this.solvedCheck("number");
		return this.num;
	}
	//Returns the sum o the digits of the number
	public getSum(): number{
		this.solvedCheck("sum");
		return this.sumOfElements;
	}
}


/* Results:
2^1000 = 10715086071862673209484250490600018105614048117055336074437503883703510511249361224931983788156958581275946729175531468251871452856923140435984577574698574803934567774824230985421074605062371141877954182153046474983581941267398767559165543946077062914571196477686542167660429831652624386837205668069376
The sum of the elements is 1366
It took an average of 7.985 microseconds to run this problem through 100 iterations
*/
