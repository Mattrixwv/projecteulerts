//ProjectEulerTS/Problems/Problem7.ts
//Matthew Ellison
// Created: 03-10-21
//Modified: 07-14-21
//What is the 10001th prime number?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { Problem } from "./Problem";
import { getNumPrimes } from "../../../Typescript/typescriptClasses/NumberAlgorithms";


export class Problem7 extends Problem{
	//Variables
	//Static variables
	private static NUMBER_OF_PRIMES: number = 10001;	//The number of primes we are trying to get
	//Instance variables
	private primes: number[];

	//Functions
	//Constructor
	public constructor(){
		super(`What is the ${Problem7.NUMBER_OF_PRIMES}th prime number?`);
	}
	//Operational functions
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Setup the variables
		this.primes = getNumPrimes(Problem7.NUMBER_OF_PRIMES);	//Holds the prime numbers


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		this.solved = true;
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.primes = [];
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `The ${Problem7.NUMBER_OF_PRIMES}th prime number is ${this.getPrime()}`;
	}
	//Returns the requested prime number
	public getPrime(): number{
		this.solvedCheck("prime");
		return this.primes[this.primes.length - 1];
	}
}


/* Results:
The 10001th prime number is 104743
It took an average of 3.534 milliseconds to run this problem through 100 iterations
*/
