//ProjectEulerTS/Problems/Problem35.ts
//Matthew Ellison
// Created: 06-06-21
//Modified: 07-14-21
//How many circular primes are there below one million?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { getPrimes } from "../../../Typescript/typescriptClasses/NumberAlgorithms";
import { Problem } from "./Problem";


export class Problem35 extends Problem{
	//Variables
	//Static variables
	private static MAX_NUM: number = 999999;	//The largest number that we are checking for primes
	//Instance variables
	private primes: number[];	//The primes below MAX_NUM
	private circularPrimes: number[];	//The circular primes below MAX_NUM
	//Functions
	//Returns a list of all rotations of a string passed to it
	private getRotations(str: string): string[]{
		let rotations: string[] = [];
		rotations.push(str);
		for(let cnt = 1;cnt < str.length;++cnt){
			str = str.substring(1) + str[0];
			rotations.push(str);
		}
		return rotations;
	}
	//Constructor
	public constructor(){
		super("How many circular primes are there below one million?");
		this.primes = [];
		this.circularPrimes = [];
	}
	//Operational functions
	//Solve the problem
	public solve(){
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Get all primes under 1,000,000
		this.primes = getPrimes(Problem35.MAX_NUM);
		//Go through all primes, get all their rotations, and check if htose numbers are also primes
		for(let cnt = 0;cnt < this.primes.length;++cnt){
			let prime = this.primes[cnt];
			let allRotationsPrime: boolean = true;
			//Get all of the rotations of the prime and see if they are also prime
			let rotations: string[] = this.getRotations(prime.toString());
			for(let rotCnt = 0;rotCnt < rotations.length;++rotCnt){
				let rotation = rotations[rotCnt];
				let p: number = parseInt(rotation);
				if(!this.primes.includes(p)){
					allRotationsPrime = false;
					break;
				}
			}
			//If all rotations are prime add it to the list of circular primes
			if(allRotationsPrime){
				this.circularPrimes.push(prime);
			}
		}


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		this.solved = true;
	}
	//Reset the problem so it can be run again
	public reset(){
		super.reset();
		this.primes = [];
		this.circularPrimes = [];
	}
	//Gets
	//Returns a string with the solution to the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `The number of all circular prime numbers under ${Problem35.MAX_NUM} is ${this.circularPrimes.length}`;
	}
	//Returns the array of primes < MAX_NUM
	public getPrimes(): number[]{
		this.solvedCheck("list of primes");
		return this.primes;
	}
	//Returns the array of circular primes < MAX_NUM
	public getCircularPrimes(): number[]{
		this.solvedCheck("list of circular primes");
		return this.circularPrimes;
	}
	//Returns the number of circular primes < MAX_NUM
	public getNumCircularPrimes(): number{
		this.solvedCheck("number oc circular primes");
		return this.circularPrimes.length;
	}
}


/* Results:
The number of all circular prime numbers under 999999 is 55
It took an average of 7.204 seconds to run this problem through 100 iterations
*/
