//ProjectEulerTS/Problems/Problem11.ts
//Matthew Ellison
// Created: 03-24-21
//Modified: 07-14-21
//Find the sum of all the primes below two million
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { Problem } from "./Problem";
import { getProd } from "../../../Typescript/typescriptClasses/ArrayAlgorithms";


export class Problem11 extends Problem{
	//Variables
	//Static variables
	//This is the grid of numbers that we will be working with
	private static grid: number[][] =  [[ 8,  2, 22, 97, 38, 15,  0, 40,  0, 75,  4,  5,  7, 78, 52, 12, 50, 77, 91,  8],
										[49, 49, 99, 40, 17, 81, 18, 57, 60, 87, 17, 40, 98, 43, 69, 48,  4, 56, 62,  0],
										[81, 49, 31, 73, 55, 79, 14, 29, 93, 71, 40, 67, 53, 88, 30,  3, 49, 13, 36, 65],
										[52, 70, 95, 23,  4, 60, 11, 42, 69, 24, 68, 56,  1, 32, 56, 71, 37,  2, 36, 91],
										[22, 31, 16, 71, 51, 67, 63, 89, 41, 92, 36, 54, 22, 40, 40, 28, 66, 33, 13, 80],
										[24, 47, 32, 60, 99,  3, 45,  2, 44, 75, 33, 53, 78, 36, 84, 20, 35, 17, 12, 50],
										[32, 98, 81, 28, 64, 23, 67, 10, 26, 38, 40, 67, 59, 54, 70, 66, 18, 38, 64, 70],
										[67, 26, 20, 68,  2, 62, 12, 20, 95, 63, 94, 39, 63,  8, 40, 91, 66, 49, 94, 21],
										[24, 55, 58,  5, 66, 73, 99, 26, 97, 17, 78, 78, 96, 83, 14, 88, 34, 89, 63, 72],
										[21, 36, 23,  9, 75,  0, 76, 44, 20, 45, 35, 14,  0, 61, 33, 97, 34, 31, 33, 95],
										[78, 17, 53, 28, 22, 75, 31, 67, 15, 94,  3, 80,  4, 62, 16, 14,  9, 53, 56, 92],
										[16, 39,  5, 42, 96, 35, 31, 47, 55, 58, 88, 24,  0, 17, 54, 24, 36, 29, 85, 57],
										[86, 56,  0, 48, 35, 71, 89,  7,  5, 44, 44, 37, 44, 60, 21, 58, 51, 54, 17, 58],
										[19, 80, 81, 68,  5, 94, 47, 69, 28, 73, 92, 13, 86, 52, 17, 77,  4, 89, 55, 40],
										[ 4, 52,  8, 83, 97, 35, 99, 16,  7, 97, 57, 32, 16, 26, 26, 79, 33, 27, 98, 66],
										[88, 36, 68, 87, 57, 62, 20, 72,  3, 46, 33, 67, 46, 55, 12, 32, 63, 93, 53, 69],
										[ 4, 42, 16, 73, 38, 25, 39, 11, 24, 94, 72, 18,  8, 46, 29, 32, 40, 62, 76, 36],
										[20, 69, 36, 41, 72, 30, 23, 88, 34, 62, 99, 69, 82, 67, 59, 85, 74,  4, 36, 16],
										[20, 73, 35, 29, 78, 31, 90,  1, 74, 31, 49, 71, 48, 86, 81, 16, 23, 57,  5, 54],
										[ 1, 70, 54, 71, 83, 51, 54, 69, 16, 92, 33, 48, 61, 43, 52,  1, 89, 19, 67, 48]];
	//Instance variables
	private greatestProduct: number[];	//Holds the largest product we have found so far

	//Functions
	//Constructor
	public constructor(){
		super("What is the greatest product of four adjacent numbers in the same direction (up, down, left, right, or diagonally) in the 20×20 grid?");
		this.greatestProduct = [];
	}
	//Operational functions
	//Solve the problem
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Holds the numbers we are currently working on
		let currentProduct: number[] = [];

		//Make sure all elements hae at least 4 elements
		for(let cnt = 0;cnt < 4;++cnt){
			this.greatestProduct.push(0);
			currentProduct.push(0);
		}

		//Start the timer
		this.timer.start();


		//Loop through every row and column
		for(let row = 0;row < Problem11.grid.length;++row){
			for(let col = 0;col < Problem11.grid[row].length;++col){
				//Directional booleans to show whether you can move a certain direction
				let left = false;
				let right = false;
				let down = false;

				//Check which direction you will be able to move
				if((col - 3) >= 1){
					left = true;
				}
				if((col + 3) < Problem11.grid[row].length){
					right = true;
				}
				if((row + 3) < 20){
					down = true;
				}

				//Check the direction you are able to go
				//Right
				if(right){
					//Fill the product
					currentProduct[0] = Problem11.grid[row][col];
					currentProduct[1] = Problem11.grid[row][col + 1];
					currentProduct[2] = Problem11.grid[row][col + 2];
					currentProduct[3] = Problem11.grid[row][col + 3];

					//If the current number's product is greater than the greatest product replace it
					if(getProd(currentProduct) > getProd(this.greatestProduct)){
						this.greatestProduct = [...currentProduct];
					}
				}
				//Down
				if(down){
					//Fill the product
					currentProduct[0] = Problem11.grid[row][col];
					currentProduct[1] = Problem11.grid[row + 1][col];
					currentProduct[2] = Problem11.grid[row + 2][col];
					currentProduct[3] = Problem11.grid[row + 3][col];

					//If the current number's product is greater than the greatest product replace it
					if(getProd(currentProduct) > getProd(this.greatestProduct)){
						this.greatestProduct = [...currentProduct];
					}
				}
				//LeftDown
				if(left && down){
					//Fill the product
					currentProduct[0] = Problem11.grid[row][col];
					currentProduct[1] = Problem11.grid[row + 1][col - 1];
					currentProduct[2] = Problem11.grid[row + 2][col - 2];
					currentProduct[3] = Problem11.grid[row + 3][col - 3];

					//If the current number's product is greater than the greatest product replace it
					if(getProd(currentProduct) > getProd(this.greatestProduct)){
						this.greatestProduct = [...currentProduct];
					}
				}
				//RightDown
				if(right && down){
					//Fill the product
					currentProduct[0] = Problem11.grid[row][col];
					currentProduct[1] = Problem11.grid[row + 1][col + 1];
					currentProduct[2] = Problem11.grid[row + 2][col + 2];
					currentProduct[3] = Problem11.grid[row + 3][col + 3];

					//If the current number's product is greater than the greatest product replace it
					if(getProd(currentProduct) > getProd(this.greatestProduct)){
						this.greatestProduct = [...currentProduct];
					}
				}
			}
		}


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		this.solved = true;
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.greatestProduct = [];
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `The greatest product of 4 numbers in a  line is ${getProd(this.greatestProduct)}\nThe numbers are ${this.greatestProduct}`;
	}
	//Returns the numbers that were being searched
	public getNumbers(): number[]{
		this.solvedCheck("numbers");
		return this.greatestProduct;
	}
	//Returns teh product that was requested
	public getProduct(): number{
		this.solvedCheck("product of the numbers");
		return getProd(this.greatestProduct);
	}
}


/* Results:
The greatest product of 4 numbers in a  line is 70600674
The numbers are 89,94,97,87
It took an average of 24.310 microseconds to run this problem through 100 iterations
*/
