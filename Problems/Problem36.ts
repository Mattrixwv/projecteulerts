//ProjectEulerTS/Problems/Problem36.ts
//Matthew Ellison
// Created: 06-29-21
//Modified: 07-14-21
//Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { getSum } from "../../../Typescript/typescriptClasses/ArrayAlgorithms";
import { toBin } from "../../../Typescript/typescriptClasses/NumberAlgorithms";
import { isPalindrome } from "../../../Typescript/typescriptClasses/StringAlgorithms";
import { Problem } from "./Problem";


export class Problem36 extends Problem{
	//Variables
	//Static variables
	private static MAX_NUM: number = 999999;	//The largest number that will be checked
	//Instance variables
	private palindromes: number[];	//All numbers that are aplindromes in base 10 and 2
	private sum: number;	//The sum of all elements in the list of palindromes

	//Functions
	//Constructor
	public constructor(){
		super("Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.");
		this.palindromes = [];
		this.sum = 0;
	}
	//Operational functions
	//Solve the problems
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Start with 1, check if it is a palindrome in base 10 and 2, and continue to MAX_NUM
		for(let num = 1;num < Problem36.MAX_NUM;++num){
			//Check if num is a palindrome
			if(isPalindrome(num.toString())){
				//Convert num to base 2 and see if that is a palindrome
				let binNum: string = toBin(num);
				if(isPalindrome(binNum)){
					//Add num to the list of palindromes
					this.palindromes.push(num);
				}
			}
		}
		//Get the sum of all palindromes in the list
		this.sum = getSum(this.palindromes);


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		this.solved = true;
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.palindromes = [];
		this.sum = 0;
	}
	//Gets
	//Returns a string with the solution to the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `The sum of all base 10 and base 2 palindromic numbers < ${Problem36.MAX_NUM} is ${this.sum}`;
	}
	//Return the array of palindromes < MAX_NUM
	public getPalindromes(){
		this.solvedCheck("list of palindromes");
		return this.palindromes;
	}
	//Returns the sum of all elements in the array of palindromes
	public getSumOfPalindromes(){
		this.solvedCheck("sum of all palindromes");
		return this.sum;
	}
}


/* Results:
The sum of all base 10 and base 2 palindromic numbers < 999999 is 872187
It took an average of 297.957 milliseconds to run this problem through 100 iterations
*/
