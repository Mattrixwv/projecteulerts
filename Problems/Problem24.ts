//ProjectEulerTS/Problems/Problem24.ts
//Matthew Ellison
// Created: 04-08-21
//Modified: 07-14-21
//What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { getPermutations } from "../../../Typescript/typescriptClasses/StringAlgorithms";
import { Problem } from "./Problem";


export class Problem24 extends Problem{
	//Variables
	//Static variables
	private static NEEDED_PERM: number = 1000000;	//The number of the permutation that you need
	private static nums: string = "0123456789";		//All of the characters that we need to go the permutations of
	//Instance variables
	private permutations: string[];	//Holds all of the permutations of the string nums

	//Functions
	//Constructor
	public constructor(){
		super(`What is the millionth lexicographic permutation of the digits ${Problem24.nums}?`);
		this.permutations = [];
	}
	//Operational functions
	//Solve the problems
	public solve(): void{
		//If the problem has already been solved do nothing and end the function
		if(this.solved){
			return;
		}

		//Start the timer
		this.timer.start();


		//Get all the permutations of the string
		this.permutations = getPermutations(Problem24.nums);


		//Stop the timer
		this.timer.stop();

		//Throw a flag to show the problem is solved
		this.solved = true;
	}
	//Reset the problem so it can be run again
	public reset(): void{
		super.reset();
		this.permutations = [];
	}
	//Gets
	//Returns the result of solving the problem
	public getResult(): string{
		this.solvedCheck("result");
		return `The 1 millionth permutation is ${this.permutations[Problem24.NEEDED_PERM - 1]}`;
	}
	//Returns an array with all of the permutations
	public getPermutationsList(): string[]{
		this.solvedCheck("permutations");
		return this.permutations;
	}
	//Returns the requested permutation
	public getPermutation(){
		this.solvedCheck("1,000,000th permutation");
		return this.permutations[Problem24.NEEDED_PERM - 1];
	}
}


/* Results:
The 1 millionth permutation is 2783915460
It took an average of 1.946 seconds to run this problem through 100 iterations
*/
