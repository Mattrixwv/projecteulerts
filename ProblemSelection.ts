//ProjectEulerTS/ProblemSelection.ts
//Matthew Ellison
// Created: 10-18-20
//Modified: 03-10-21
//This class holds all of the functions needed to handle a problem
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/typescriptClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


const readlineSync = require("readline-sync");
import { Problem } from "./Problems/Problem";
import { Problem1  } from "./Problems/Problem1";
import { Problem2  } from "./Problems/Problem2";
import { Problem3  } from "./Problems/Problem3";
import { Problem4  } from "./Problems/Problem4";
import { Problem5  } from "./Problems/Problem5";
import { Problem6  } from "./Problems/Problem6";
import { Problem7  } from "./Problems/Problem7";
import { Problem8  } from "./Problems/Problem8";
import { Problem9  } from "./Problems/Problem9";
import { Problem10 } from "./Problems/Problem10";
import { Problem11 } from "./Problems/Problem11";
import { Problem12 } from "./Problems/Problem12";
import { Problem13 } from "./Problems/Problem13";
import { Problem14 } from "./Problems/Problem14";
import { Problem15 } from "./Problems/Problem15";
import { Problem16 } from "./Problems/Problem16";
import { Problem17 } from "./Problems/Problem17";
import { Problem18 } from "./Problems/Problem18";
import { Problem19 } from "./Problems/Problem19";
import { Problem20 } from "./Problems/Problem20";
import { Problem21 } from "./Problems/Problem21";
import { Problem22 } from "./Problems/Problem22";
import { Problem23 } from "./Problems/Problem23";
import { Problem24 } from "./Problems/Problem24";
import { Problem25 } from "./Problems/Problem25";
import { Problem26 } from "./Problems/Problem26";
import { Problem27 } from "./Problems/Problem27";
import { Problem28 } from "./Problems/Problem28";
import { Problem29 } from "./Problems/Problem29";
import { Problem30 } from "./Problems/Problem30";
import { Problem31 } from "./Problems/Problem31";
import { Problem32 } from "./Problems/Problem32";
import { Problem33 } from "./Problems/Problem33";
import { Problem34 } from "./Problems/Problem34";
import { Problem35 } from "./Problems/Problem35";
import { Problem36 } from "./Problems/Problem36";
import { Problem37 } from "./Problems/Problem37";
import { Problem67 } from "./Problems/Problem67";


export class ProblemSelection{
	//Holds the valid problem numbers
	public static PROBLEM_NUMBERS: number[] = [  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
												21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 67];

	//Returns the problem corresponding to the given problem number
	public static getProblem(problemNumber: number): Problem{
		let problem: Problem  = null;
		switch(problemNumber){
			case 1 : problem = new Problem1(); break;
			case 2 : problem = new Problem2(); break;
			case 3 : problem = new Problem3(); break;
			case 4 : problem = new Problem4(); break;
			case 5 : problem = new Problem5(); break;
			case 6 : problem = new Problem6(); break;
			case 7 : problem = new Problem7(); break;
			case 8 : problem = new Problem8(); break;
			case 9 : problem = new Problem9(); break;
			case 10: problem = new Problem10(); break;
			case 11: problem = new Problem11(); break;
			case 12: problem = new Problem12(); break;
			case 13: problem = new Problem13(); break;
			case 14: problem = new Problem14(); break;
			case 15: problem = new Problem15(); break;
			case 16: problem = new Problem16(); break;
			case 17: problem = new Problem17(); break;
			case 18: problem = new Problem18(); break;
			case 19: problem = new Problem19(); break;
			case 20: problem = new Problem20(); break;
			case 21: problem = new Problem21(); break;
			case 22: problem = new Problem22(); break;
			case 23: problem = new Problem23(); break;
			case 24: problem = new Problem24(); break;
			case 25: problem = new Problem25(); break;
			case 26: problem = new Problem26(); break;
			case 27: problem = new Problem27(); break;
			case 28: problem = new Problem28(); break;
			case 29: problem = new Problem29(); break;
			case 30: problem = new Problem30(); break;
			case 31: problem = new Problem31(); break;
			case 32: problem = new Problem32(); break;
			case 33: problem = new Problem33(); break;
			case 34: problem = new Problem34(); break;
			case 35: problem = new Problem35(); break;
			case 36: problem = new Problem36(); break;
			case 37: problem = new Problem37(); break;
			case 67: problem = new Problem67(); break;
		}
		return problem;
	}
	//Print the description of a problem
	public static printDescription(problemNumber: number): void{
		//Get the problem
		let problem: Problem = ProblemSelection.getProblem(problemNumber);
		//Print the problem's description
		console.log(problem.getDescription() + "\n\n");
	}
	//Solve a problem
	public static solveProblem(problemNumber: number): void{
		//Get the problem
		let problem: Problem = ProblemSelection.getProblem(problemNumber);
		//Print the problem description
		console.log("\n\n" + problem.getDescription());
		//Solve the problem
		problem.solve();
		//Print the results
		console.log(problem.getResult() + "\nIt took " + problem.getTime() + " to solve this problem.\n\n");
	}
	//Get a valid problem number from a user
	public static getProblemNumber(): number{
		let problemNumber: number = 0;
		let prob = readlineSync.question("Enter a problem number: ");
		try{
			problemNumber = parseInt(prob);
		}
		catch(error){
			problemNumber = -1;
		}
		while(!ProblemSelection.PROBLEM_NUMBERS.includes(problemNumber)){
			console.log("That is an invalid problem number!");
			problemNumber = ProblemSelection.getProblemNumber();
		}
		problemNumber = parseInt(prob);
		return problemNumber;
	}
	//List all valid problem numbers
	public static listProblems(): void{
		console.log(ProblemSelection.PROBLEM_NUMBERS[1]);
		for(let problemNumber: number = 2;problemNumber < ProblemSelection.PROBLEM_NUMBERS.length;++problemNumber){
			console.log(", " + ProblemSelection.PROBLEM_NUMBERS[problemNumber]);
		}
		console.log();
	}
}
